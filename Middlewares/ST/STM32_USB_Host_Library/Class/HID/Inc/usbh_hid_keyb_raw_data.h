/*
 * usbh_hid_keyb_raw_data.h
 *
 *  Created on: May 9, 2022
 *      Author: Michal
 */

#ifndef ST_STM32_USB_HOST_LIBRARY_CLASS_HID_INC_USBH_HID_KEYB_RAW_DATA_H_
#define ST_STM32_USB_HOST_LIBRARY_CLASS_HID_INC_USBH_HID_KEYB_RAW_DATA_H_

/* Includes ------------------------------------------------------------------*/
#include "usbh_hid.h"



typedef struct
{
  uint8_t data[8];
}
HID_KEYBD_Data_TypeDef;

HID_KEYBD_Data_TypeDef	   keybd_data;
uint32_t                   keybd_rx_report_buf[2];
uint32_t                   keybd_report_data[2];


USBH_StatusTypeDef USBH_HID_KeybdInit(USBH_HandleTypeDef *phost);
HID_KEYBD_Data_TypeDef *USBH_HID_GetKeybdData(USBH_HandleTypeDef *phost);


#endif /* ST_STM32_USB_HOST_LIBRARY_CLASS_HID_INC_USBH_HID_KEYB_RAW_DATA_H_ */
