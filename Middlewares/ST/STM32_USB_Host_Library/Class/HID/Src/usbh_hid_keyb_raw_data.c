/*
 * usbh_hid_keyb_raw_data.c
 *
 *  Created on: May 9, 2022
 *      Author: Michal
 */


/* Includes ------------------------------------------------------------------*/
#include "usbh_hid_keyb_raw_data.h"
#include "usbh_hid_parser.h"



const HID_Report_ItemTypedef imp_0_full_array =
{
  (uint8_t *)(void *)keybd_report_data + 0, /*data*/
  8,     /*size*/
  0,     /*shift*/
  8,     /*count (only for array items)*/
  0,     /*signed?*/
  0,     /*min value read can return*/
  255,   /*max value read can return*/
  0,     /*min vale device can report*/
  255,   /*max value device can report*/
  1      /*resolution*/
};

/**
  * @brief  USBH_HID_KeybdInit
  *         The function init the HID keyboard.
  * @param  phost: Host handle
  * @retval USBH Status
  */
USBH_StatusTypeDef USBH_HID_KeybdInit(USBH_HandleTypeDef *phost)
{
  uint32_t x;
  HID_HandleTypeDef *HID_Handle = (HID_HandleTypeDef *) phost->pActiveClass->pData;


  for (x = 0U; x < (sizeof(keybd_report_data) / sizeof(uint32_t)); x++)
  {
    keybd_report_data[x] = 0U;
    keybd_rx_report_buf[x] = 0U;
  }

  if (HID_Handle->length > (sizeof(keybd_report_data)))
  {
    HID_Handle->length = (sizeof(keybd_report_data));
  }
  HID_Handle->pData = (uint8_t *)(void *)keybd_rx_report_buf;
  USBH_HID_FifoInit(&HID_Handle->fifo, phost->device.Data, HID_QUEUE_SIZE * sizeof(keybd_report_data));

  return USBH_OK;
}

/**
  * @brief  USBH_HID_KeybdDecode
  *         The function decode keyboard data.
  * @param  phost: Host handle
  * @retval USBH Status
  */


static USBH_StatusTypeDef USBH_HID_KeybdDecode(USBH_HandleTypeDef *phost)
{
  uint8_t x;

  HID_HandleTypeDef *HID_Handle = (HID_HandleTypeDef *) phost->pActiveClass->pData;
  if (HID_Handle->length == 0U)
  {
    return USBH_FAIL;
  }

  /*Fill report */
  if (USBH_HID_FifoRead(&HID_Handle->fifo, &keybd_report_data, HID_Handle->length) ==  HID_Handle->length)
  {



    for (x = 0U; x < 8; x++)
    {
      keybd_data.data[x] = (uint8_t)HID_ReadItem((HID_Report_ItemTypedef *) &imp_0_full_array, x);
    }

    return USBH_OK;
  }
  return   USBH_FAIL;
}

/**
  * @brief  USBH_HID_GetKeybdInfo
  *         The function return keyboard information.
  * @param  phost: Host handle
  * @retval keyboard information
  */
HID_KEYBD_Data_TypeDef *USBH_HID_GetKeybdData(USBH_HandleTypeDef *phost)
{
  if (USBH_HID_KeybdDecode(phost) == USBH_OK)
  {
    return &keybd_data;
  }
  else
  {
    return NULL;
  }
}
